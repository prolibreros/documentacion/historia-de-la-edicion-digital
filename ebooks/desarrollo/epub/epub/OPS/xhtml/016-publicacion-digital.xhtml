<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" xml:lang="es" lang="es">
    <head>
        <meta charset="UTF-8" />
        <title>Publicación digital</title>
        <link rel="stylesheet" href="../css/styles.css" />
    </head>
    <body epub:type="chapter">
        <h1>Publicación digital</h1>
        <p>La víspera del próximo milenio involucró el advenimiento de lo que actualmente conocemos como «publicación digital». Aunque en otra entrada se tratará con mayor detenimiento la historia de los <em>ebooks</em>, cabe destacar que los primeros formatos pensados para su lectura desde pantallas aparecieron en 1999.</p>
        <p>A finales del milenio se publicaron las especificaciones de la OEBPS (<em>Open eBook Publication Structure</em>, por su sigla en inglés). Esto sentó las bases para el <a href="https://en.wikipedia.org/wiki/Open_eBook">Open eBook</a> y, posteriormente, para la OPS (<em>Open Publication Structure</em>, por su sigla en inglés), la estructura del <a href="https://en.wikipedia.org/wiki/EPUB">EPUB</a>. Otros formatos privativos y populares como los empleados por <a href="https://en.wikipedia.org/wiki/Amazon_Kindle#File_formats">Amazon</a> o <a href="https://es.wikipedia.org/wiki/IBooks">Apple</a> han basado su desarrollo en la estructura del EPUB o del Open eBook.</p>
        <figure>
            <img src="../img/interiores/epub.jpg" />
            <figcaption>Figura 12. Logotipo del EPUB, formato mantenido por el International Digital Publishing Forum (<a href="http://idpf.org/">IDPF</a>). Fuente: <a href="https://en.wikipedia.org/wiki/File:EPUB_logo.svg">Wikimedia Commons</a>.</figcaption>
        </figure>
        <p>Si bien es cierto que el surgimiento de las publicaciones digitales sentó un precedente en la edición digital, este no se debe al formato en sí ni al concepto de publicación digital. La relevancia de las publicaciones digitales reside en que hace patente repensar la pertinencia del enfoque WYSIWYG dentro de los procesos editoriales, sin importar que el formato de salida sea una publicación impresa o digital.</p>
        <p>La idea de <em>publicación digital</em> existe mucho antes de la aparición de los formatos desarrollados específicamente para cumplir esta función. Así como el escritor y el editor empezaron a usar el procesador de texto con fines distintos al trabajo administrativo, asimismo el usuario en general empezó a utilizar ciertos formatos para otra función distinta; a saber, la lectura a través de una pantalla. Los archivos de texto plano, del procesador de texto, las páginas web y los PDF han sido empleados como publicaciones digitales <em>de facto</em>.</p>
        <p>En 1971 nació el <a href="https://es.wikipedia.org/wiki/Proyecto_Gutenberg">proyecto Gutenberg</a>, la primera iniciativa para crear una biblioteca digital a partir de libros impresos. Nótese el nacimiento prematuro de la digitalización de obras que, a pesar de ser una de las primeras directrices de los procesos digitales de edición, aún hoy en día sigue considerándose un proceso transversal que pocos editores le prestan atención. En la mayoría de los casos, la digitalización se percibe más como un procedimiento técnico que uno editorial.</p>
        <figure>
            <img src="../img/interiores/proyectogutenberg.jpg" />
            <figcaption>Figura 13. Logotipo del proyecto Gutenberg. Fuente: <a href="https://en.wikipedia.org/wiki/File:Project_Gutenberg_logo.png">Wikimedia Commons</a>.</figcaption>
        </figure>
        <p>Si se relacionan los procesadores de texto, la edición digital y el libro electrónico puede decirse que: la idea con mayor antigüedad es la del procesador de texto como <em>hardware</em> (años sesenta), en seguida la del libro electrónico (años setenta), después la del procesador de texto como <em>software</em> (años ochenta) y, por último, la de edición digital (años noventa). La noción de libro electrónico es anterior a la de los programas de procesamiento de texto y, evidentemente, más añeja que el concepto de edición digital.</p>
        <p>En los setenta, mientras que los primeros programadores centraban sus esfuerzos en producir sistemas operativos cada vez más completos, los científicos continuaban explorando el potencial de procesamiento de las computadoras y los militares investigaban la pertinencia de esta tecnología para fines bélicos. Michael Hart, un estudiante de la Universidad de Illinois, utilizó una de las computadoras del Laboratorio de Investigación de Materiales para transcribir la primera publicación del proyecto Gutenberg: la Declaración de Independencia de los Estados Unidos. Incluso Hart relata que esto aconteció el 4 de julio de 1971, durante el aniversario de la independencia estadounidense… Cierto o no, la anécdota demuestra cómo el optimismo y entusiasmo por crear una biblioteca digital no precisó de la madurez tecnológica para crearla: sin escáneres, sin procesadores de texto, solo un editor de texto plano y tiempo libre.</p>
        <p>El libro electrónico nació precipitadamente, sin respaldo tecnológico, sin intereses lucrativos, solo con el puro afán de ofrecer libros de manera digital y gratuita. Este espíritu, en mayor o menor medida, es la explicación de por qué el libro electrónico existió treinta y ocho años antes del surgimiento del primer formato desarrollado para satisfacer esta necesidad. Pero ¿por qué tuvieron que pasar casi cuatro décadas para el nacimiento de las <em>publicaciones digitales</em> como las conocemos en la actualidad?</p>
        <p>Al hacerse libros electrónicos con diversos formatos, como los mencionados antes, no se percibió la necesidad de crear un archivo <em>específico</em> para cumplir con esta tarea. Esto evidencia la herencia tecnológica que ha acarreado la edición y la publicación en la era digital: el uso de herramientas pensadas para otros fines y la relevancia al aspecto visual en lugar de la estructura del contenido.</p>
        <p>Los primeros libros electrónicos fueron elaborados con la ausencia de resalte tipográfico. Posteriormente, estos fueron implementando características ortotipográficas desde el enfoque WYSIWYG que permitían los procesadores de texto. De nueva cuenta, los libros electrónicos compuestos con TeX representaron una minoría. En la mayoría de los casos estas circunstancias acarrearon una ausencia del cuidado editorial, perceptible en errores en la transcripción o la digitalización, descuido tipográfico, ausencia de composición y demás cuestiones en detrimento de la legibilidad del texto.</p>
        <p>Para combatir estas fallas, surgió la necesidad de un formato específico para las publicaciones digitales cuya producción no involucrara un enfoque WYSIWYG. Como el enfoque WYSIWYM no surgiría hasta principios del milenio, los lenguajes de etiquetas fueron la respuesta a este dilema. A finales de los noventa los lenguajes de marcado más desarrollados eran TeX, HTML y XML, estos dos últimos creados para lo que desde hace algunos años ya era una realidad: la <a href="https://en.wikipedia.org/wiki/World_Wide_Web"><em>web</em></a>.</p>
        <figure>
            <img src="../img/interiores/primerapagina.jpg" />
            <figcaption>Figura 14. Captura de pantalla del explorador utilizado para la visualización de la <a href="http://info.cern.ch/hypertext/WWW/TheProject.html">primera página</a><a href="http://info.cern.ch/hypertext/WWW/TheProject.html"><em>web</em></a>. Fuente: <a href="http://media.npr.org/assets/img/2013/05/21/screensnap2_24c_custom-991377675cc755770fee182040d4bc3d6d00ba69-s300-c85.gif">NPR</a>.</figcaption>
        </figure>
        <p>Mientras el mundo editorial asimilaba el traslado digital de la mayoría de su quehacer, el internet iniciaba su expansión más allá de los límites académicos y militares. El surgimiento del internet, en donde curiosamente el Laboratorio de Investigación de Materiales de la Universidad de Illinois formó parte de los primeros nodos, produjo el nacimiento de la web en 1991. Esta utilizaba como archivos de intercambio los documentos de hipertexto.</p>
        <p>El lenguaje HTML nació como el medio para crear marcas de hipertexto. La rápida evolución de la web propició que el HTML se consolidara como uno de los lenguajes de marcado más populares, cuya principal característica es la posibilidad de presentar inconsistencias en la sintaxis sin ocasionar graves problemas durante la visualización. Esta flexibilidad puede ocasionar dificultades al momento de utilizar este lenguaje de marcado como un archivo para el procesamiento automatizado de información. Con el fin de solucionar este inconveniente, se creó el <a href="https://es.wikipedia.org/wiki/Extensible_Markup_Language">XML</a>, un lenguaje de marcado más estricto. Entre uno y otro formato, en el 2000 nació un lenguaje de marcado para su visualización, tal como el HTML, que exige una sintaxis clara, como el XML; a saber, el <a href="https://es.wikipedia.org/wiki/XHTML">XHTML</a>.</p>
        <p>Este conjunto de formatos, todos bajo el cuidado del W3C, permitieron una rápida propagación del internet gracias a su capacidad de transmitir información de una manera clara para el usuario y consistente para las computadoras. Esta doble ventaja no pasó desapercibida ante quienes buscaban crear libros electrónicos desde un enfoque distinto al WYSIWYG.</p>
        <p>A partir de 1999 y principalmente desde el 2007, cuando sale a la luz la primera versión de EPUB, las publicaciones digitales estandarizadas contemplan las siguientes características mínimas que permiten la creación de un libro electrónico más versátil y de mayor calidad editorial:</p>
        <ul>
            <li>El uso del lenguaje XML para la creación de la estructura y metadatos del libro. Por ejemplo, para indicar el título y autor de la obra, manifestar los archivos existentes en el libro, señalar el orden de lectura o crear la tabla de contenidos.</li>
            <li>El uso del lenguaje XHTML, aunque también el HTML es soportado, para el contenido del libro. Por ejemplo, los archivos para el prólogo, los capítulos, el epílogo o los apéndices.</li>
            <li>La compresión de este árbol de directorios para crear un solo archivo que fácilmente puede compartirse.</li>
        </ul>
        <figure>
            <img src="../img/interiores/sexochilango.jpg" />
            <figcaption>Figura 15. Captura de pantalla de la estructura y el contenido de <em>Sexo Chilango</em>, un libro publicado por Nieve de Chamoy. Fuente: <a href="http://www.nievedechamoy.com.mx/sitio/">Nieve de Chamoy</a>.</figcaption>
        </figure>
        <p>Estos formatos de publicación digital en sí mismos demuestran ser los más óptimos, porque por el uso del lenguaje de marcado permite que los lectores de libros organicen de una mejor manera el contenido al mismo tiempo que el lector tiene la posibilidad de enriquecer su experiencia de lectura. Sin embargo, en el mundo editorial su acogida no fue como se esperaba. Algunos de los factores que permiten explicar la tardía o desinteresada recepción de la publicación digital dentro de los procesos digitales de edición pueden ser:</p>
        <ol>
            <li>la continua disputa entre la lucha de formatos: impreso <em>vs</em> digital,</li>
            <li>el desdén ocasionado por la baja calidad que presentan muchos libros electrónicos,</li>
            <li>el costo adicional que implica la producción de un libro electrónico o</li>
            <li>la reticencia de modificar los procesos de edición digital.</li>
        </ol>
        <p>En futuros artículos se hablará sobre los primeros dos puntos. Respecto a los puntos restantes, efectivamente para la gran mayoría de las editoriales la creación de un libro electrónico involucra una inversión adicional ya que la metodología empleada para la producción de libros impresos dista mucho del manejo de etiquetas necesario para el desarrollo de <em>ebooks</em>. Para que la publicación digital forme parte «natural» dentro de los procesos de edición digital es necesario que estos paulatinamente dejen de lado el enfoque WYSIWYG.</p>
        <p>La suspicacia ante dicho traslado es comprensible. Durante los noventa el sector editorial se vio en la necesidad de adoptar la edición digital como el principal método para la producción de libros. Una década después se le indica al mundo del libro que de nueva cuenta ha de acoger otra metodología para la publicación de libros, sean impresos o digitales.</p>
        <figure>
            <img src="../img/interiores/procesoseditoriales.jpg" />
            <figcaption>Figura 16. Esquema de los principales procesos editoriales, en horizontal, los considerados fundamentales y en vertical, los estimados como transversales. Fuente: <a href="https://github.com/ColectivoPerroTriste">Perro Triste</a>.</figcaption>
        </figure>
        <p>De manera paralela a esta discusión endémica al mundo de la edición, grandes distribuidores de productos digitales —algunos de ellos completamente ajenos a la comercialización de libros, como Google o Apple, u otro con explícitas intenciones de modificar el mercado del libro, como Amazon— han apostado desde hace algunos años por el libro electrónico. Esta intromisión no es mínima: al tiempo que se modifican las reglas tradicionales para la venta de libros también ha cambiado la forma de interacción del lector con el escritor y de este con el editor. De modo paralelo, las editoriales se han visto en la necesidad de publicar libros electrónicos a marchas forzadas para satisfacer este nuevo mercado.</p>
        <p>Bajo este panorama, quienes mejor han logrado adaptarse a este cambio son los jóvenes lectores y las personas que se autopublican. La primera década del nuevo milenio puede decirse que fue el inicio de la lucha del sector editorial con los fantasmas de su pasado.</p>
    </body>
</html>
