<?xml version="1.0" encoding="UTF-8"?>
<package xmlns="http://www.idpf.org/2007/opf" xml:lang="es" unique-identifier="uid" prefix="ibooks: http://vocabulary.itunes.apple.com/rdf/ibooks/vocabulary-extensions-1.0/" version="3.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/">
        <dc:title>Historia de la edición digital</dc:title>
        <dc:creator>Santa Ana Anguiano, Ramiro</dc:creator>
        <dc:publisher>Nieve de Chamoy y Mariana Eguaras</dc:publisher>
        <dc:description>Breve historia sobre cómo los procesos editoriales han adoptado las tecnologías digitales.</dc:description>
        <dc:subject>Artículo, Divulgación</dc:subject>
        <dc:language>es</dc:language>
        <dc:identifier id="uid">Historia de la edición digital-1.3.0</dc:identifier>
        <meta property="dcterms:modified">2016-10-24T23:28:22Z</meta>
        <meta property="rendition:layout">reflowable</meta>
        <meta property="ibooks:specified-fonts">true</meta>
    </metadata>
    <manifest>
        <item href="css/styles.css" id="id_styles_css" media-type="text/css" />
        <item href="img/interiores/IBMmtst.jpg" id="id_IBMmtst_jpg" media-type="image/jpeg" />
        <item href="img/interiores/IBMpublicidad.jpg" id="id_IBMpublicidad_jpg" media-type="image/jpeg" />
        <item href="img/interiores/bibtex.jpg" id="id_bibtex_jpg" media-type="image/jpeg" />
        <item href="img/interiores/epub.jpg" id="id_epub_jpg" media-type="image/jpeg" />
        <item href="img/interiores/estiloparrafo.jpg" id="id_estiloparrafo_jpg" media-type="image/jpeg" />
        <item href="img/interiores/flujoepub.jpg" id="id_flujoepub_jpg" media-type="image/jpeg" />
        <item href="img/interiores/formateo.jpg" id="id_formateo_jpg" media-type="image/jpeg" />
        <item href="img/interiores/gitbook.jpg" id="id_gitbook_jpg" media-type="image/jpeg" />
        <item href="img/interiores/gitramas.jpg" id="id_gitramas_jpg" media-type="image/jpeg" />
        <item href="img/interiores/gitrepositorio.jpg" id="id_gitrepositorio_jpg" media-type="image/jpeg" />
        <item href="img/interiores/libreoffice.jpg" id="id_libreoffice_jpg" media-type="image/jpeg" />
        <item href="img/interiores/lyx.jpg" id="id_lyx_jpg" media-type="image/jpeg" />
        <item href="img/interiores/markdown.jpg" id="id_markdown_jpg" media-type="image/jpeg" />
        <item href="img/interiores/microsoftPublicidad.jpg" id="id_microsoftPublicidad_jpg" media-type="image/jpeg" />
        <item href="img/interiores/nestededitor.jpg" id="id_nestededitor_jpg" media-type="image/jpeg" />
        <item href="img/interiores/pagemaker.jpg" id="id_pagemaker_jpg" media-type="image/jpeg" />
        <item href="img/interiores/primerapagina.jpg" id="id_primerapagina_jpg" media-type="image/jpeg" />
        <item href="img/interiores/procesoseditoriales.jpg" id="id_procesoseditoriales_jpg" media-type="image/jpeg" />
        <item href="img/interiores/processedworld01.jpg" id="id_processedworld01_jpg" media-type="image/jpeg" />
        <item href="img/interiores/processedworld12.jpg" id="id_processedworld12_jpg" media-type="image/jpeg" />
        <item href="img/interiores/proyectogutenberg.jpg" id="id_proyectogutenberg_jpg" media-type="image/jpeg" />
        <item href="img/interiores/scribus.jpg" id="id_scribus_jpg" media-type="image/jpeg" />
        <item href="img/interiores/sexochilango.jpg" id="id_sexochilango_jpg" media-type="image/jpeg" />
        <item href="img/interiores/tecnologiasweb.jpg" id="id_tecnologiasweb_jpg" media-type="image/jpeg" />
        <item href="img/interiores/wysiwygYlatex.jpg" id="id_wysiwygYlatex_jpg" media-type="image/jpeg" />
        <item href="img/logo.png" id="id_logo_png" media-type="image/png" />
        <item href="img/logo2.png" id="id_logo2_png" media-type="image/png" />
        <item href="img/logo3.jpg" id="id_logo3_jpg" media-type="image/jpeg" />
        <item href="img/portada.jpg" id="id_portada_jpg" media-type="image/jpeg" properties="cover-image" />
        <item href="toc/nav.xhtml" id="id_nav_xhtml" media-type="application/xhtml+xml" properties="nav" />
        <item href="toc/toc.ncx" id="id_toc_ncx" media-type="application/x-dtbncx+xml" />
        <item href="xhtml/001-portadilla.xhtml" id="id_001-portadilla_xhtml" media-type="application/xhtml+xml" />
        <item href="xhtml/002-legal.xhtml" id="id_002-legal_xhtml" media-type="application/xhtml+xml" />
        <item href="xhtml/003-primera-parte.xhtml" id="id_003-primera-parte_xhtml" media-type="application/xhtml+xml" />
        <item href="xhtml/004-irrupcion-digital.xhtml" id="id_004-irrupcion-digital_xhtml" media-type="application/xhtml+xml" />
        <item href="xhtml/005-gestacion.xhtml" id="id_005-gestacion_xhtml" media-type="application/xhtml+xml" />
        <item href="xhtml/006-nacimiento.xhtml" id="id_006-nacimiento_xhtml" media-type="application/xhtml+xml" />
        <item href="xhtml/007-primeros-pasos.xhtml" id="id_007-primeros-pasos_xhtml" media-type="application/xhtml+xml" />
        <item href="xhtml/008-segunda-parte.xhtml" id="id_008-segunda-parte_xhtml" media-type="application/xhtml+xml" />
        <item href="xhtml/009-de-la-oficina-a-la-edicion-digital.xhtml" id="id_009-de-la-oficina-a-la-edicion-digital_xhtml" media-type="application/xhtml+xml" />
        <item href="xhtml/010-gestacion-paralela.xhtml" id="id_010-gestacion-paralela_xhtml" media-type="application/xhtml+xml" />
        <item href="xhtml/011-analfabetismo-digital.xhtml" id="id_011-analfabetismo-digital_xhtml" media-type="application/xhtml+xml" />
        <item href="xhtml/012-wysiwyg-y-los-lenguajes-de-etiquetas.xhtml" id="id_012-wysiwyg-y-los-lenguajes-de-etiquetas_xhtml" media-type="application/xhtml+xml" />
        <item href="xhtml/013-desktop-publishing.xhtml" id="id_013-desktop-publishing_xhtml" media-type="application/xhtml+xml" />
        <item href="xhtml/014-wysiwym.xhtml" id="id_014-wysiwym_xhtml" media-type="application/xhtml+xml" />
        <item href="xhtml/015-y-el-software-libre.xhtml" id="id_015-y-el-software-libre_xhtml" media-type="application/xhtml+xml" />
        <item href="xhtml/016-publicacion-digital.xhtml" id="id_016-publicacion-digital_xhtml" media-type="application/xhtml+xml" />
        <item href="xhtml/017-single-source-publishing.xhtml" id="id_017-single-source-publishing_xhtml" media-type="application/xhtml+xml" />
        <item href="xhtml/018-online-publishing.xhtml" id="id_018-online-publishing_xhtml" media-type="application/xhtml+xml" />
        <item href="xhtml/019-balance-del-analisis.xhtml" id="id_019-balance-del-analisis_xhtml" media-type="application/xhtml+xml" />
    </manifest>
    <spine toc="id_toc_ncx">
        <itemref idref="id_001-portadilla_xhtml"/>
        <itemref idref="id_002-legal_xhtml"/>
        <itemref idref="id_003-primera-parte_xhtml"/>
        <itemref idref="id_004-irrupcion-digital_xhtml"/>
        <itemref idref="id_005-gestacion_xhtml"/>
        <itemref idref="id_006-nacimiento_xhtml"/>
        <itemref idref="id_007-primeros-pasos_xhtml"/>
        <itemref idref="id_008-segunda-parte_xhtml"/>
        <itemref idref="id_009-de-la-oficina-a-la-edicion-digital_xhtml"/>
        <itemref idref="id_010-gestacion-paralela_xhtml"/>
        <itemref idref="id_011-analfabetismo-digital_xhtml"/>
        <itemref idref="id_012-wysiwyg-y-los-lenguajes-de-etiquetas_xhtml"/>
        <itemref idref="id_013-desktop-publishing_xhtml"/>
        <itemref idref="id_014-wysiwym_xhtml"/>
        <itemref idref="id_015-y-el-software-libre_xhtml"/>
        <itemref idref="id_016-publicacion-digital_xhtml"/>
        <itemref idref="id_017-single-source-publishing_xhtml"/>
        <itemref idref="id_018-online-publishing_xhtml"/>
        <itemref idref="id_019-balance-del-analisis_xhtml"/>
    </spine>
</package>
