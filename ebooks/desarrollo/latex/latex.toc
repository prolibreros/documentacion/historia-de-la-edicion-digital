\select@language {spanish}
\contentsline {part}{PRIMERA PARTE}{1}{section*.2}
\contentsline {chapter}{\numberline {1}Irrupci\IeC {\'o}n digital}{2}{chapter.1}
\contentsline {chapter}{\numberline {2}Gestaci\IeC {\'o}n}{5}{chapter.2}
\contentsline {chapter}{\numberline {3}Nacimiento}{7}{chapter.3}
\contentsline {chapter}{\numberline {4}Primeros pasos}{9}{chapter.4}
\contentsline {part}{SEGUNDA PARTE}{11}{section*.3}
\contentsline {chapter}{\numberline {5}De la oficina a la edici\IeC {\'o}n digital}{12}{chapter.5}
\contentsline {chapter}{\numberline {6}Gestaci\IeC {\'o}n paralela}{14}{chapter.6}
\contentsline {chapter}{\numberline {7}Analfabetismo digital}{16}{chapter.7}
\contentsline {chapter}{\numberline {8}WYSIWYG y los lenguajes de etiquetas}{20}{chapter.8}
\contentsline {chapter}{\numberline {9}\emph {Desktop publishing}}{23}{chapter.9}
\contentsline {chapter}{\numberline {10}WYSIWYM}{26}{chapter.10}
\contentsline {chapter}{\numberline {11}\IeC {\textquestiondown }Y el \emph {software} libre?}{30}{chapter.11}
\contentsline {chapter}{\numberline {12}Publicaci\IeC {\'o}n digital}{35}{chapter.12}
\contentsline {chapter}{\numberline {13}\emph {Single source publishing}}{45}{chapter.13}
\contentsline {chapter}{\numberline {14}\emph {Online publishing\vspace \bigskipamount }}{57}{chapter.14}
\contentsline {chapter}{Balance del an\IeC {\'a}lisis}{61}{section*.4}
