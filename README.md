# Historia de la edición digital

Aquí se encuentran todos los archivos empleados para desarrollar los *ebooks*
del artículo «Historia de la edición digital», editado por Nieve de Chamoy y
Mariana Eguaras, y [publicado en el *blog*](http://marianaeguaras.com/historia-de-la-edicion-digital/) de esta última.

<hr />

# Índice

* [Disponibilidad](#disponibilidad)
* [Metodología empleada](#metodología-empleada)
* [Árbol de directorios](#Árbol-de-directorios)
  * [Este repositorio](#este-repositorio)
  * [Repositorio para la versión GitBook](#repositorio-para-la-versión-gitbook)
* [Contactos](#contactos)
  * [Nieve de Chamoy](#nieve-de-chamoy)
  * [Mariana Eguaras](#mariana-eguaras)
* [Limitación de responsabilidad](#limitación-de-responsabilidad)
* [Licencia](#licencia)

<hr />

# Disponibilidad

La publicación tiene las siguientes opciones de descarga y visualización:

* EPUB: [clic para descargar](https://github.com/NikaZhenya/historia-de-la-edicion-digital/blob/master/ebooks/produccion/historia-de-la-edicion-digital.epub?raw=true).
* MOBI: [clic para descargar](https://github.com/NikaZhenya/historia-de-la-edicion-digital/blob/master/ebooks/produccion/historia-de-la-edicion-digital.mobi?raw=true).
* PDF: [clic para descargar](https://github.com/NikaZhenya/historia-de-la-edicion-digital/raw/master/ebooks/produccion/historia-de-la-edicion-digital.pdf).
* GitBook: [clic para visualizar](https://nikazhenya.gitbooks.io/historia-de-la-edicion-digital/content/).

# Metodología empleada

Los distintos formatos de «Historia de la edición digital» se crearon según la
propuesta que se indica en este artículo; una propuesta que puede resultar la
más conveniente para la mayoría de las publicaciones: la metodología del *single
source* y *online publishing*. Para conocer más sobre esta metodología, te
invitamos a leer esta breve obra

¿Quieres conocer más de cerca cómo se desarrolló esta publicación? En una
futura entrada se mostrará, con imágenes y comentarios, la manera en como estos
archivos fueron creados.

# Árbol de directorios

## Este repositorio

* `ebooks`. Contiene todo lo relacionado a las publicaciones creadas.
  * `desarrollo`. Archivos editables de las publicaciones.
    * `archivo-madre.md`. Archivo desde el cual se crearon todos los formatos de *ebook*, a partir de una conversión de `entrada-formateada.odt` mediante [`Pandoc`](http://pandoc.org/).
    * `epub`. Proyecto del EPUB creado desde cero y con los [*scripts* de Perro Triste](https://github.com/ColectivoPerroTriste/Herramientas); el archivo MOBI se desarrolló mediante una conversión del EPUB con [`KindleGen`](https://www.amazon.com/gp/feature.html?docId=1000765211).
    * `latex`. Proyecto del PDF creado con [`LaTeX`](http://www.latex-project.org/).
  * `produccion`. Archivos finales que son enlazados en diversos sitios para su descarga.
    * [`historia-de-la-edicion-digital.epub`](https://github.com/NikaZhenya/historia-de-la-edicion-digital/blob/master/ebooks/produccion/historia-de-la-edicion-digital.epub?raw=true).
    * [`historia-de-la-edicion-digital.mobi`](https://github.com/NikaZhenya/historia-de-la-edicion-digital/blob/master/ebooks/produccion/historia-de-la-edicion-digital.mobi?raw=true).
    * [`historia-de-la-edicion-digital.pdf`](https://github.com/NikaZhenya/historia-de-la-edicion-digital/raw/master/ebooks/produccion/historia-de-la-edicion-digital.pdf).
* `recursos`. Archivos base para empezar el desarrollo de las publicaciones.
  * `img`. Imágenes interiores.
  * `textos`. Textos empleados para elaborar el `archivo-madre.md`.
    * `entrada-editada.rtf`. Entrada editada que entregó Mariana Eguaras.
    * `entrada-formateada.odt`. Entrada formateada para estructurar el contenido.
* `.gitignore`. Archivo que indica a [`git`](https://git-scm.com/) qué documentos ignorar; en este caso, archivos basura.
* `README.md`. ¡Este archivo que estás leyendo!

## Repositorio para la versión GitBook

El libro de GitBook es [otro repositorio](https://github.com/NikaZhenya/historia-de-la-edicion-digital-gitbook) de GitHub que es visualizado como una publicación. Su estructura es la siguiente:

* `capitulos`. Esta carpeta contiene cada uno de los capítulos de esta publicación en formato [`Markdown`](http://daringfireball.net/projects/markdown/); se trata del `archivo-madre.md` dividido por secciones y con las imágenes enlazadas.
* `img`. Contiene todas las imágenes para esta publicación.
* `.gitignore`. Archivo que indica a [`git`](https://git-scm.com/) qué documentos ignorar; en este caso, archivos basura.
* `README.md`. La portadilla del *ebook*.
* `book.json`. Archivo para personalizar el libro; en este caso se indicó el lenguaje y en `indice.md` se establece el orden de lectura.
* `indice.md`. La tabla de contenidos de la publicación.

# Contactos

## Nieve de Chamoy

[www.nievedechamoy.com.mx](http://www.nievedechamoy.com.mx)

[contacto@nievedechamoy.com.mx](mailto:contacto@nievedechamoy.com.mx)

## Mariana Eguaras

[www.marianaeguaras.com](http://marianaeguaras.com/)

[hola@marianaeguaras.com](mailto:hola@marianaeguaras.com)

# Limitación de responsabilidad

**El presente artículo solo es para fines de divulgación. No pretende ser exhaustivo y se enfoca en la publicación de libros electrónicos estándar.**

**Primera edición, 2016.**

***Última revisión: 28 de septiembre de 2016.***

# Licencia

Todos los archivos están bajo [Licencia Editorial Abierta y Libre (LEAL)](http://www.leal.perrotriste.io).
